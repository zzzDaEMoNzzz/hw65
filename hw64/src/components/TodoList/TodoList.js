import React, {Fragment} from 'react';
import TodoTask from "../TodoTask/TodoTask";

const TodoList = ({tasks, deleteTask}) => {
  const todoList = tasks.map(item => (
    <TodoTask
      key={item.id}
      task={item.task}
      deleteTask={() => deleteTask(item.id)}
    />
  ));

  return (
    <Fragment>
      {todoList}
    </Fragment>
  );
};

export default TodoList;
