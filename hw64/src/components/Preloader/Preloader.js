import React, {Fragment} from 'react';
import './Preloader.css';

const Preloader = () => {
  return (
    <Fragment>
      <div className="Preloader" />
    </Fragment>
  );
};

export default Preloader;
