import React, {Fragment} from 'react';
import ToWatchFilm from "../ToWatchFilm/ToWatchFilm";

const ToWatchList = ({films, deleteFilm, saveChanges}) => {
  const filmsList = films.map(item => (
    <ToWatchFilm
      key={item.id}
      id={item.id}
      film={item.film}
      deleteFilm={deleteFilm}
      saveChanges={saveChanges}
    />
  ));

  return (
    <Fragment>
      {filmsList}
    </Fragment>
  );
};

export default ToWatchList;
