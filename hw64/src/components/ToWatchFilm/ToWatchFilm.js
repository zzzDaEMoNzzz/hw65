import React, {Component} from 'react';
import './ToWatchFilm.css';

class ToWatchFilm extends Component {
  state = {
    inputValue: this.props.film
  };

  inputOnChange = event => {
    this.setState({inputValue: event.target.value});
  };

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return this.state.inputValue !== nextState.inputValue;
  }

  render() {
    return (
      <div className="ToWatchFilm">
        <input
          type="text"
          value={this.state.inputValue}
          onChange={this.inputOnChange}
          onBlur={() => this.props.saveChanges(this.props.id, this.state.inputValue)}
        />
        <button className="ToWatchFilm-deleteBtn" onClick={() => this.props.deleteFilm(this.props.id)}>Delete</button>
      </div>
    );
  }
}

export default ToWatchFilm;
