import React, {Component} from 'react';
import axios from 'axios';
import TodoAdd from "../../components/TodoAdd/TodoAdd";
import TodoList from "../../components/TodoList/TodoList";
import Preloader from "../../components/Preloader/Preloader";

class Todo extends Component {
  state = {
    tasks: [],
    inputValue: '',
    loading: true,
  };

  inputOnChangeHandler = event => this.setState({inputValue: event.target.value});

  getTasks = () => {
    axios.get('todo.json').then(response => {
      if (response.data && Object.keys(response.data).length > 0) {
        const tasksIDs = Object.keys(response.data);
        const tasks = tasksIDs.reduce((array, id) => {
          array.push({id: id, ...response.data[id]});
          return array;
        }, []);

        this.setState({tasks});
      } else this.setState({tasks: []});
    }).finally(() => this.setState({loading: false}));
  };

  addTask = event => {
    event.preventDefault();

    axios.post('https://kurlov-hw64-64d41.firebaseio.com/todo.json', {
      task: this.state.inputValue
    }).then(() => {
      this.setState({inputValue: ''});
      this.getTasks();
    });
  };

  deleteTask = id => {
    axios.delete(`todo/${id}.json`).then(() => {
      this.getTasks();
    });
  };

  componentDidMount() {
    this.getTasks();
  }

  render() {
    if (this.state.loading) return <Preloader />;

    return (
      <div>
        <TodoAdd
          inputValue={this.state.inputValue}
          inputOnChange={this.inputOnChangeHandler}
          formOnSubmit={this.addTask}
        />
        <TodoList
          tasks={[...this.state.tasks].reverse()}
          deleteTask={this.deleteTask}
        />
      </div>
    );
  }
}

export default Todo;