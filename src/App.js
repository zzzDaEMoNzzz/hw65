import React, { Component } from 'react';
import {Switch, Route} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Page from "./containers/Page/Page";
import Admin from "./containers/Admin/Admin";
import './App.css';

import axios from 'axios';
axios.defaults.baseURL = 'https://kurlov-hw65-c2c4c.firebaseio.com/';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Layout>
          <Switch>
            <Route path="/" exact component={Page} />
            <Route path="/pages/admin" exact component={Admin}/>
            <Route path="/pages/:name" component={Page}/>
            <Route render={() => (<div>Something went wrong</div>)}/>
          </Switch>
        </Layout>
      </div>
    );
  }
}

export default App;
