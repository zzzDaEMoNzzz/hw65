import React, {Component} from 'react';
import axios from 'axios';
import './Admin.css';

class Admin extends Component {
  state = {
    selectValue: '',
    pages: [],
    pageTitle: '',
    pageContent: '',
  };

  onSelectChange = event => {
    this.setState({selectValue: event.target.value});
    this.getPageInfo(event.target.value);
  };

  onTitleChange = event => this.setState({pageTitle: event.target.value});
  onContentChange = event => this.setState({pageContent: event.target.value});

  getPageInfo = pageName => {
    axios.get(`pages/${pageName}.json`).then(response => {
      this.setState({
        pageTitle: response.data.title,
        pageContent: response.data.content
      });
    });
  };

  savePage = () => {
    axios.put(`pages/${this.state.selectValue}.json`, {
      title: this.state.pageTitle,
      content: this.state.pageContent
    }).then(() => {
      this.props.history.push(`/pages/${this.state.selectValue}`);
    });
  };

  componentDidMount() {
    axios.get('pages.json').then(response => {
      const pages = Object.keys(response.data).map(pageName => {
        return {id: pageName, ...response.data[pageName]};
      });

      this.setState({pages});
    });
  }

  render() {
    const selectOptions = this.state.pages.map(page => {
      return (<option key={page.id} value={page.id}>{page.title}</option>);
    });

    return (
      <div className="Admin">
        <h3>Edit Pages</h3>
        <select value={this.state.selectValue} onChange={this.onSelectChange}>
          <option value="" hidden disabled>Select Page</option>
          {selectOptions}
        </select>
        <label htmlFor="pageTitle">Title</label>
        <input id="pageTitle" type="text" value={this.state.pageTitle} onChange={this.onTitleChange}/>
        <label htmlFor="pageContent">Content</label>
        <textarea id="pageContent" rows="10" value={this.state.pageContent} onChange={this.onContentChange}/>
        <button onClick={this.savePage} disabled={this.state.selectValue === ''}>Save</button>
      </div>
    );
  }
}

export default Admin;