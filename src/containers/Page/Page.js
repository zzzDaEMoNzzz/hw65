import React, {Component} from 'react';
import axios from "axios";
import './Page.css';

class Page extends Component {
  state = {
    title: '',
    content: ''
  };

  getPageInfo = () => {
    let pageName = 'home';

    if (this.props.match.params.name) {
      pageName = this.props.match.params.name;
    }

    axios.get(`pages/${pageName}.json`).then(response => {
      this.setState({...response.data});
    });
  };

  componentDidMount() {
    this.getPageInfo();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      this.getPageInfo();
    }
  }

  render() {
    return (
      <div className="Page">
        <h3>{this.state.title}</h3>
        <p>{this.state.content}</p>
      </div>
    );
  }
}

export default Page;