import React from 'react';
import {Link, NavLink, withRouter} from "react-router-dom";

import './Header.css';

const Header = props => {
  const isHomePage = () => {
    return props.location.pathname === '/' || props.location.pathname === '/pages/home';
  };

  return (
    <header className="Header">
      <Link to="/">Static Pages</Link>
      <nav>
        <NavLink to="/pages/home" isActive={isHomePage}>Home</NavLink>
        <NavLink to="/pages/about">About</NavLink>
        <NavLink to="/pages/contacts">Contacts</NavLink>
        <NavLink to="/pages/page4">Page-4</NavLink>
        <NavLink to="/pages/page5">Page-5</NavLink>
        <NavLink to="/pages/admin">Admin</NavLink>
      </nav>
    </header>
  );
};

export default withRouter(Header);
